#coding: utf-8

import sys

def get_input():
    input_list = []
    while 1:
        print("数字を入力してください、終わったらenterキーを入力")
        input_num = sys.stdin.readline()
        if input_num == "\n":
            break
        else:
            if input_num.strip().isdigit():
                input_list.append(int(input_num))

            # 入力が数字ではなかった場合再び入力を促す
            else:
                print("数字以外のものが入力されています")
    return input_list

def bubble_sort(sort_list):
    for i in range(len(sort_list)):

        # 外側のループが1周するごとに、sort_listの末尾から順にソートが完了していく
        # そのため内側のループの範囲が外側が一周するごとに減っていく
        for j in range(len(sort_list)-i-1):
            if sort_list[j] > sort_list[j+1]:

                # 二項の値を同時に交換している
                sort_list[j], sort_list[j+1] = sort_list[j+1], sort_list[j]
    return sort_list

def quick_sort(sort_list):
    if len(sort_list) == 0 or len(sort_list) == 1:
        return sort_list
    else:
        axis = sort_list[0]
        smaller_list = []
        bigger_list = []
        for i in range(1, len(sort_list)):

            # リストの先頭を基準にして、
            # それとの大小関係によって二つのリストに分ける
            if sort_list[i] <= axis:
                smaller_list.append(sort_list[i])
            else:
                bigger_list.append(sort_list[i])
        return quick_sort(smaller_list) + [axis] + quick_sort(bigger_list)
def merge_sort(sort_list):
    def merge(a_list, b_list):
        i = 0
        j = 0
        merge_list = []

        # 二つのリストの先頭の大小を比較し、小さいものを新しいリストに追加し、
        # 追加したものindexを一つ増やす。どちらかのリストが最後尾までくると、
        # 残りのリストの要素を新しいリストに追加する。 
        while 1:
            if j == len(b_list):
                merge_list.extend(a_list[i:])  #a.extend(b)はaにbの要素を追加する
                break
            elif i == len(a_list):
                merge_list.extend(b_list[j:])
                break
            elif a_list[i] <= b_list[j]:
                merge_list.append(a_list[i])
                i += 1
            elif a_list[i] > b_list[j]:
                merge_list.append(b_list[j])
                j += 1
        return merge_list
    if len(sort_list) == 1:
        return sort_list
    else:

        # //は切り捨てて割り算を行うので結果がint型になる
        a = merge_sort(sort_list[:len(sort_list)//2])
        b = merge_sort(sort_list[len(sort_list)//2:])
        return merge(a, b)
def main():
    sort_list = get_input()
    print("入力リスト")
    print(sort_list)
    print ("バブルソート")

    # sort_list[:]はsort_listをコピーしている
    # コピーには浅いコピーと深いコピーがあるので注意！！！
    # 参考URL: http://qiita.com/mas9612/items/65ba04bf0dd9ade125c0
    print(bubble_sort(sort_list[:]))
    print("クイックソート")
    print(quick_sort(sort_list[:]))
    print("マージソート")
    print(merge_sort(sort_list[:]))
main()

