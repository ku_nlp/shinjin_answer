#coding: utf-8

import sys

def main():
    input_list = []
    while 1:
        print("数字を入力してください、終わったらenterキーを入力")
        input_num = sys.stdin.readline()
        if input_num == "\n":
            break
        else:
            if input_num.strip().isdigit():
                input_list.append(int(input_num))

            # 入力が数字ではなかった場合再び入力を促す
            else:
                print("数字以外のものが入力されています")

    # range(a, b)はiがaからb-1まで増えていく。
    # len(input_list)は要素の数を表しており、
    # input_listの最大のindexはlen(input_list)-1になっている。
    for i in range(1, len(input_list)):
        input_list[i] = 0

    print(input_list)

main()
