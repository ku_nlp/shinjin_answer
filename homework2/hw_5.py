#coding: utf-8

import sys


def gcd(a, b):
    while 1:
        if a < b:     #aとbの大小関係を考慮して場合分け
            a, b = b, a
        if a % b == 0:      #あまりが0ならbを返す
            break
        else:
            a, b = b, a % b
    return b
def reflexive_gcd(a, b):     #再帰的に行う場合
    if a < b:
        a, b =b, a
    if a % b == 0:
        return b
    else:
        return reflexive_gcd(b, a%b)

def get_input():
    while 1:
        print("自然数を入力してください")
        inp = sys.stdin.readline()
        if inp.strip().isdigit():
            inp = int(inp)
            break
        else:
            print("数字でないものが入力されています")
    return inp


def main():
    a = get_input()
    b = get_input()
    gcf = gcd(a, b)
    print("最大公約数：", end = "")
    print(gcf)
    print("再帰バージョン")
    a = get_input()
    b = get_input()
    print("最大公約数：", end = "")
    print(reflexive_gcd(a, b))


main()
