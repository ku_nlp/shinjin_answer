#!/usr/bin/env python3
#coding:utf-8
import math
import time
import sys
n = int(sys.argv[1])

#エラトステネスの篩
#自然数nが√nを越えない最大の整数以下の全ての素数で割り切れなければ，nは素数である。
#素数は無限に存在する。
#任意の合成数は，ただ1通りの方法で素数の積の形に表すことができる。
#O(nlog(logn))
def era1(n):
    num = list(range(0,n+1))#1
    num[0] = num[1] = 0#2
    for i in range(2,int(n**0.5)+2):#3
        if num[i] != 0:#4
            for j in range(i**2,n+1,i):#5
                num[j] = 0
    return [i for i in range(2,n+1) if num[i] != 0]#6
#1 [0,1,2,3, ... ,n+1]
#2 [0,0,2,3, ... ,n+1]
#3 numにはindexと同じ数字が入っている
#なので停止条件として決め打ちで
#rangeを(2,2,int(n**0.5)+1)としている。
#もちろんrange(2,n+1)if i >= n**0.5:breakもOK
#4 0を素数でない条件に使用
#5 iは残して次のi*2から倍数を0にして削除
#例えばi=2の時には、
#[0,0,2,3,0,5,0,7,0,9,0...]となっている。
#6 最後に0でない場所に残った数字（素数）を出力

#エラトステネスの篩２
#より高速な実装方式
#http://sota1235.com/blog/2014/04/14/14151429.html
def era2(n):
    num = [True]*(n+1)
    num[0] = num[1] = False
    for i in range(2,int(n**0.5)+1):
        if num[i]:
            for j in range(i**2, n+1, i):
                num[j] = False
    return [i for i in range(2,n+1) if num[i]]
#数字の部分を1,0のフラグに変えている
#数字で扱うのが最後の配列のみなので高速

#エラトステネスの篩３
#東山くんのやり方
def era3(n):
    num = list(range(2,n+1))
    for i in num:
        if i > n ** 0.5:
            break
        for j in num:
            if j != i and j%i == 0:
                num.remove(j)
                #remove処理が遅い
    return num

#removeを使わずに処理した場合
def era4(n):
    num = list(range(2,n+1))
    for i in num:
        if i > n ** 0.5:
            break
        for j in range(len(num)):
            if num[j] == 0 or i == 0:
                continue
            if num[j] != i and num[j]%i == 0:
                num[j]=0
    return [i for i in num if i !=0]

#前回の課題方式1
#O(n**2)
def pre1(n):
    prime = [2]
    for i in range(2,n+1):
        for k in range(2,i):
            if i%k == 0:
                break
            elif k == i-1:#1
                prime.append(i)
    return prime

#前回の課題方式２
#中尾くんのやり方
def pre2(n):
    prime = []
    for i in range(2,n+1):
        isprime = True
        for k in range(2,i):
            if i%k == 0:#2
                isprime = False
                break
        if isprime:
            prime.append(i)
    return prime
#pre1に比べてelifを判定してないので高速
#1と2を比較

#前回の課題方式の改良
#割り算の停止条件にi**0.5以下の
#場合の数のみ割るを追加
#計算量はO(n**3/2)
def pre3(n):
    prime = []
    for i in range(2,n+1):
        isprime = True
        for k in range(2,int(i**0.5)+1):
            if i%k == 0:#2
                isprime = False
                break
        if isprime:
            prime.append(i)
    return prime


t0 = time.clock()
print(len(era1(n)))
t1 = time.clock()

t2 = time.clock()
print(len(era2(n)))
t3 = time.clock()

t4 = time.clock()
print(len(era3(n)))
t5 = time.clock()

t6 = time.clock()
print(len(era4(n)))
t7 = time.clock()

t8 = time.clock()
print(len(pre1(n)))
t9 = time.clock()

t10 = time.clock()
print(len(pre2(n)))
t11 = time.clock()

t12 = time.clock()
print(len(pre3(n)))
t13 = time.clock()


print("エラトステネスの篩1 dt={0:.3f}[s]".format(t1-t0))
print("エラトステネスの篩2 dt={0:.3f}[s]".format(t3-t2))
print("エラトステネスの篩3 dt={0:.3f}[s]".format(t5-t4))
print("エラトステネスの篩4 dt={0:.3f}[s]".format(t7-t6))
print("前回の課題手法1 dt={0:.3f}[s]".format(t9-t8))
print("前回の課題手法2 dt={0:.3f}[s]".format(t11-t10))
print("前回の課題手法の改良 dt={0:.3f}[s]".format(t13-t12))
