#!/usr/bin/env python3
#coding:utf-8
import time

#8クイーン問題
#8つのクイーンの置き方は64C8=4426165368通りであり膨大
#しかし実際のところ、同じ行と列にはおけないので、
#8!=40320通りを調べれば良いことになる。
#さらに斜めの利き筋に関しては、
#行と列の位置を足す、または行から列を引くと
#一定の値になることを利用すれば、楽にチェックが可能。
boards = []
def check(n,board):
    #boardの1からnまでのコマが衝突しているか調べる
    for y in range(1, n):
        #もし衝突していた場合、ダメなのでfalseを返す
        if conflict(board,board[y],y):
            return False
    return True

def conflict(board,x,y):
    #指定したコマのxとyと、それ以前のコマが衝突しているか
    #初めから逐一調査。条件は残る斜めの効き筋のみ
    #もしも自分のコマのxとyの合計もしくは差が
    #他のコマのxとyの合計もしくは差に一致した場合
    #衝突しているのでTrueを返す
    for y1 in range(0, y):
        x1 = board[y1]
        if x1 - y1 == x - y or x1 + y1 == x + y:
            return True
    return False

def queen(n,board,y = 0):
    if n == y:
        boards.append(board)
    else:
        for x in range(0, n):
            if x in board or conflict(board,x,y):
                continue
            #高速化する場合
            #結果が出てから衝突をチェックするのではなく
            #生成するときにすでに衝突を判定して、
            #衝突するコマ配置を生成しないようにする。
            #board.append(x)
            queen(n,board+[x],y + 1)
            #board.pop()

def printboard():
    for board in boards:
        print(board)
        print("-"*18)
        for i in board:
            print("| "*i+"|Q"+"| "*(7-i)+"|")
        print("-"*18)
            
t10 = time.clock()
queen(8,[],y=0)
print(len(boards))
t11 = time.clock()
printboard()
print("{0:.4f}[s]".format(t11-t10))


#!python3
from itertools import permutations
n = 8
#縦と横の利き筋を考えると、8!通りの順列になるので、
#まず順列をpermutationsという関数で、全通りの順列を作成
allboard = [board for board in permutations(range(n),n)]

#順列が斜めの利き筋で衝突している場合True
#衝突していない場合にFalseを返す
#衝突の判定には、斜めの利き筋がx-y=定数,x+y=定数
#なので、同じ定数が含まれている場合True
def collision(board):
    xmy = []
    xpy = []
    for x,y in enumerate(board):
        if x-y in xmy:
            return True
        elif x+y in xpy:
            return True
        xmy.append(x-y)
        xpy.append(x+y)
    else:
        return False
        
def checkall(allboard):
    count = 0
    for i in allboard:
        if not collision(i):
            count += 1
    print(count)


t0 = time.clock()
checkall(allboard)
t1 = time.clock()
print("{0:.4f}[s]".format(t1-t0))

t2 = time.clock()
print(len([board for board in permutations(range(n),n) if not collision(board)]))
t3 = time.clock()
print("{0:.4f}[s]".format(t3-t2))
