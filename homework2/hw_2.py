#!/usr/bin/env python3
#coding:utf-8


#最大値とindexを今まで習った方法で取り出す方式
def max_index1(l):
    max_temp = l[0]#最大値をリスト先頭の数字と仮定
    index_temp = 0
    for i in range(len(l)):
        if l[i] >= max_temp:
            max_temp = l[i]
            index_temp = i
    return max_temp,index_temp
#len(l)で配列の長さを表し、
#range()で長さ分のシーケンスを出力
#最終的に条件にマッチしたものは、
#最大値及びそのindexとなっている。

#一行で出力する方式
def max_index2(l):
    return max(l),l.index(max(l))
#max()で最大の要素を取り出し、index()でindexを取り出す。


#同じ値が配列に含まれていた場合に、
#全てのインデックスを出力する方式
def max_index3(l):
    indexes = [i for i, x in enumerate(l) if x == max(l)]
    return max(l),indexes
    #max()は配列から最大の要素を返す関数
    #enumerateは配列のindexと要素を同時に出力するための関数
    #なので、indexesの中では、もしもその要素が最大の要素である場合
    #indexをリストに格納するという動作をする。
    
    #内包表記を展開すると、
    #indexes = []
    #for i,x in enumerate(l):
    #    if x == max(l):
    #        indexes.append(i)
    

l = [3,44,55,66666,2,2,3,4,5,6,777,7,22,3,3]
print(max_index1(l))
print(max_index2(l))
print(max_index3(l))
