#coding: utf-8

import sys

def get_input():        #三つの数字を受け取る関数
    while 1:
        inp = sys.stdin.readline()
        if inp.strip().isdigit():   #入力が数字ではなかった場合再び入力を促す
            inp = int(inp)
            break
        else:
            print("数字を入力してください")
    return inp



def max_return(a, b, c):
    max_num = a
    if max_num <= b:
        max_num = b
    if max_num <= c:
        max_num = c
    return max_num


def main():
    print("三つの数字を入力してください")
    print("一つ目")
    a = get_input()
    print("二つ目")
    b = get_input()
    print("三つ目")
    c = get_input()
    print("最大値：" , end = "")
    print(max_return(a, b, c))

main()
