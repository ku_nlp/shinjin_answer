#!/usr/bin/env python3
#coding:utf-8
import sys
import time
import math
import numpy as np
n = int(sys.argv[1])

#再帰呼び出しする方式
def fib1(n):
    if n == 1:
        return 0
    elif n == 2:
        return 1
    else:
        return fib1(n-1)+fib1(n-2)
#fib(n)=fib(n-1)+fb(n-2)を利用して
#再帰的に計算する方法
#それぞれのfib()は順にfib(n-1)とfib(n-2)に
#分解されて、最終的にfib(1)orfib(2)になるまで再帰される
#オーダーは指数乗O(((1+5**0.5)/2)**n)

#積み上げ方式（繰り返し方式）
def fib2(n):
    a,b= 0,1
    for i in range(n):
        a,b = a+b,a
    return b
#n=1の時,a=1,b=0
#n=2の時,a=1,b=1
#n=3の時,a=2,b=1
#...というように順にn回足すことで
#フィボナッチ数を求める方法
#明らかにO(n)


t0 = time.clock()
print(fib1(n))
t1 = time.clock()
print("dt=","{0:.4f}[s]".format(t1-t0))

t2 = time.clock()
print(fib2(n))
t3 = time.clock()
print("dt=","{0:.4f}[s]".format(t3-t2))

#再帰呼び出しを使用した場合
#計算量が O( ((1 + sqrt(5)) / 2)**(n-1) ) になる
#積み上げ式の場合O(n)になる
#再帰的になるメリット実装が簡単なこと、デメリットは計算量が大きいこと
#再帰的でないメリットは計算量が少なくて済むこと、デメリットは少し煩雑なこと
